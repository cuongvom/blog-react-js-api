﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WebApplication1.Models;
using WebApplication1.ModelViews;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<AppUser> _userM;
        private readonly AccountService _service;
        private readonly IConfiguration _configuration;

        public AccountController(UserManager<AppUser> user
            , IConfiguration configuration
            )
        {
            _userM = user;
            _configuration = configuration;
            _service = new AccountService(user);

        }
        [HttpPost("register")]
        public async Task<ActionResult> RegisterAsync(RegisterViewModel register)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.user_RegisterAsync(register);
                if (result)
                    return Ok();
                return BadRequest();
            }
            return BadRequest();
        }
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.user_LoginAsync(login);
                if (result)
                {
                    var user = await _userM.FindByNameAsync(login.UserName);
                    var listrole = _userM.GetRolesAsync(user).Result.ToArray();

                    //var role = string.Join(",",listrole.ToArray());

                    var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.Name,login.UserName),
                            new Claim(JwtRegisteredClaimNames.Sub, login.UserName),
                            new Claim("id",user.Id.ToString()),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())


                        };
                    foreach (var x in listrole)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, x));
                    }
                    var token = new JwtSecurityToken
                    (
                        issuer: _configuration.GetValue<string>("TokenOptions:Issuer"),
                        audience: _configuration.GetValue<string>("TokenOptions:Audience"),
                        claims: claims,
                        expires: DateTime.Now.AddDays(30),
                        notBefore: DateTime.Now,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetValue<string>("TokenOptions:SigningKey"))),
                        SecurityAlgorithms.HmacSha256)
                    );
                    return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
                }
                return BadRequest();
            }
            return BadRequest(login);
        }
        [Authorize]
        [HttpPost("profile/changepassword")]
        public async Task<ActionResult> ChangePasswordAsync(Account_View_Model acc)
        {
            var result = await _service.change_PasswordAsync(acc);
            if (result)
                return Ok(acc.NewPassword);
            return NotFound();
        }
       
    }
}