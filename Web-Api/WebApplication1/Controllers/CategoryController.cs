﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ApplicationContext _db;
        public CategoryController(ApplicationContext db)
        {
            _db = db;
        }
        [HttpGet("Get")]
        public async Task<ActionResult<IEnumerable<Category>>> Get()
        {
            var result = await _db.Categories.ToListAsync();
            if(result!=null)
                return Ok(result);
            return NotFound();
        }
        [HttpGet("{id}")]
        public IActionResult getCategory(int id)
        {
            if (id != 0)
            {
                var data = _db.Categories.Find(id);
                if (data == null)
                    return NotFound(id);
                else
                {
                    return Ok(data);
                }
            }
            return NotFound(id);
        }
    }
}