﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;
using WebApplication1.ModelViews;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly CommentService _service;
        public CommentController(ApplicationContext db)
        {
            _service = new CommentService(db);
        }
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Comment>> Get(int id)
        {
            if (id != 0)
            {
                var list = _service.getAllbyPostID(id).ToList();
                return Ok(list);
            }
            return NotFound();
        }
        [HttpPost]
        [Authorize]
        public IActionResult  Post(Comment_View_Model comm)
        {
            if (ModelState.IsValid)
            {
                var result = _service.create_Comm(comm);
                if (result)
                    return Ok(result);
                return BadRequest();
            }
            return BadRequest();
        }
        [Authorize]
        [HttpPost("delete/{id}")]
        public IActionResult Delete(int id)
        {
            var result = _service.delete_Comm(id);
            if (result)
                return Ok();
            return NotFound();
        }
        [Authorize]
        [HttpPost("subcomment")]
        public IActionResult Post(SubComment_View_Model subcomment)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            else
            {
                var result = _service.post_subComment(subcomment);
                if (result)
                    return Ok(new { subcomment=subcomment});
                return BadRequest();
            }
        }
        [HttpGet("subcomment/{id}")]
        public ActionResult<IEnumerable<SubComment>> GetSubComment(int id)
        {
            if (id != 0)
            {
                var result = _service.getAllSubCom(id).ToList();
                return Ok(new { listcomm= result });
            }
            return BadRequest();
        }
    }
}