﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostController : ControllerBase
    {
        private readonly PostService _service;
        public PostController(ApplicationContext db)
        {
            _service = new PostService(db);
        }
        [AllowAnonymous]
        [HttpGet("Get")]
        public ActionResult<IEnumerable<Post>> Get()
        {
            var data = _service.getAllPostAsync().ToList();
            return Ok(data);
            //return await _db.Posts.OrderByDescending(x => x.PostID).ToListAsync();
        }
        [HttpGet("GetPostbyUserId")]
        public ActionResult<IEnumerable<Post>> GetPostbyUserId(string id)
        {
            var data= _service.getPost_by_UserID(id).ToList();
            return Ok(data);
            //return await _db.Posts.Where(x=>x.UserID==id)
            //.OrderByDescending(x => x.PostID).ToListAsync();
        }
        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetPostbyId(int id)
        {
            if (id != 0)
            {
                var data = _service.getPost_byID(id);
                if (data != null)
                {
                    return Ok(data);
                }
                return NotFound();
            }
            return BadRequest();

        }

        [HttpPost("Create")]
        public IActionResult Create([FromForm]PostViewModel post)
        {
            if (ModelState.IsValid)
            {
                var result = _service.create_NewPost(post);
                if (result)
                    return Ok();
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _service.delete_Post(id);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
        [HttpPut]
        public IActionResult Update([FromForm]Post_Update_View_Model post)
        {
            if (ModelState.IsValid)
            {
                var result = _service.update_Post(post);
                if (result)
                    return Ok();
                return BadRequest();
            }
            return BadRequest(post);
        }
    }
}