﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly SearchService _service;
        public SearchController(ApplicationContext db)
        {
            _service = new SearchService(db);
        }
        [HttpGet]
        public ActionResult<IEnumerable<Post>> Get(string search)
        {
            if (search != "")
            {
                var list = _service.searchbyTitle(search).ToList();
                return Ok(list);
            }
            return BadRequest();
        }

    }
}