﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class aupdatesubcomment0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AppUsersId",
                table: "SubComments",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UserID",
                table: "SubComments",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_SubComments_AppUsersId",
                table: "SubComments",
                column: "AppUsersId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubComments_AspNetUsers_AppUsersId",
                table: "SubComments",
                column: "AppUsersId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubComments_AspNetUsers_AppUsersId",
                table: "SubComments");

            migrationBuilder.DropIndex(
                name: "IX_SubComments_AppUsersId",
                table: "SubComments");

            migrationBuilder.DropColumn(
                name: "AppUsersId",
                table: "SubComments");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "SubComments");
        }
    }
}
