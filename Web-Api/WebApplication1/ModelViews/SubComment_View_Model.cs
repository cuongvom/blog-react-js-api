﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.ModelViews
{
    public class SubComment_View_Model
    {
        [Required]
        public int CommentID { get; set; }
        [Required]
        public string Content { get; set; }
    }
}
