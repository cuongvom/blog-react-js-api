﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Comment
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int CommentID { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCom { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string UserID { get; set; }
        [Required]
        public int PostID { get; set; }
        public virtual AppUser AppUser { get; set; }
        public virtual Post Post { get; set; }
    }
}
