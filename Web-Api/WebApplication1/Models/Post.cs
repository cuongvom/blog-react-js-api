﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Post
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int PostID { get; set; }
        [Required]
        public string Tittle { get; set; }
        public string Image { get; set; }
        [Required]
        public int SumComment { get; set; }
        [Required]
        public string Content { get; set; }
        [Required, DataType(DataType.DateTime)]
        public DateTime DatePost { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string UserID { get; set; }
        [Required]
        public int CategoryID { get; set; }
        public virtual AppUser AppUser { get; set; }
        public virtual Category Category { get; set; }
    }
}
