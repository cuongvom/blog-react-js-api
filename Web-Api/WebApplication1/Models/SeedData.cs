﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class SeedData
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<AppRole> _roleManager;
        public SeedData(UserManager<AppUser> user, RoleManager<AppRole> role)
        {
            _roleManager = role;
            _userManager = user;
        }
        public void DefaultData()
        {
            if (!_roleManager.Roles.Any())
            {
                _roleManager.CreateAsync(new AppRole { Name = "Admin" }).Wait();

                _roleManager.CreateAsync(new AppRole { Name = "Member" }).Wait();
            }
            if (!_userManager.Users.Any())
            {
                _userManager.CreateAsync(new AppUser { UserName = "admin" }, "12345").Wait();
                _userManager.CreateAsync(new AppUser { UserName = "cuong" }, "12345").Wait();
            }
            var user1 = _userManager.FindByNameAsync("admin").Result;
            _userManager.AddToRoleAsync(user1, "Admin").Wait();
            var user2 = _userManager.FindByNameAsync("cuong").Result;
            _userManager.AddToRoleAsync(user2, "Member").Wait();
        }
    }
}
