﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class SubComment
    {
        [Key]
        public int SubCommentID { get; set; }
        public int CommentID { get; set; }
        public string Content { get; set; }
        public DateTime DateSub { get; set; }
        public Guid UserID { get; set; }
        public virtual AppUser AppUsers {get;set;}
        public virtual Comment Comments { get; set; }
    }
}
