﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.ModelViews;

namespace WebApplication1.Services
{
    public class AccountService:IAccountService
    {
        private readonly UserManager<AppUser> _userM;
        public AccountService(UserManager<AppUser> user)
        {
            _userM = user;
        }
        public async Task<bool> user_RegisterAsync(RegisterViewModel register)
        {
            try
            {
                var user = await _userM.FindByNameAsync(register.UserName);
                if (user!=null||register.Password.Length<5)
                {
                    return false;
                }
                else
                {
                    _userM.CreateAsync(new AppUser { UserName = register.UserName }, register.Password).Wait();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> user_LoginAsync(LoginViewModel login)
        {
            try
            {
                var user = await _userM.FindByNameAsync(login.UserName);
                if (user != null)
                {
                    var checkpass = await _userM.CheckPasswordAsync(user, login.Password);
                    if (checkpass)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> change_PasswordAsync(Account_View_Model acc)
        {
            if (String.IsNullOrEmpty(acc.UserName) || String.IsNullOrEmpty(acc.Password)||String.IsNullOrEmpty(acc.NewPassword))
                return false;
            else
            {
                var user = await _userM.FindByNameAsync(acc.UserName);
                var result_change = await _userM.ChangePasswordAsync(user, acc.Password, acc.NewPassword);
                if (result_change.Succeeded)
                    return true;
                return false;
            }
        }
    }
}
