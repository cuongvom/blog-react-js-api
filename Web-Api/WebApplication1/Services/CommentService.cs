﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.ModelViews;

namespace WebApplication1.Services
{
    public class CommentService : ICommentService
    {
        private readonly ApplicationContext _db;
        public CommentService(ApplicationContext db)
        {
            _db = db;
        }
        public bool create_Comm(Comment_View_Model comm)
        {
            try
            {
                var data = new Comment()
                {
                    Content = comm.Content,
                    DateCom = DateTime.Now,
                    UserName = comm.UserName,
                    PostID = comm.PostID,
                    UserID = comm.UserID
                };
                var post = _db.Posts.Find(comm.PostID);
                if (post != null)
                {
                    post.SumComment++;
                    _db.Entry(post).State = EntityState.Modified;
                }
                else
                    return false;
                _db.Comments.Add(data);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool delete_Comm(int id)
        {
            if(id!=0)
            {
                var comm = _db.Comments.Find(id);
                if(comm!=null)
                {
                    _db.Comments.Remove(comm);
                    var post = _db.Posts.Find(comm.PostID);
                    post.SumComment--;
                    _db.Entry(post).State = EntityState.Modified;
                    _db.SaveChanges();
                    return true;
                }
                return false;
            }
            return false;
        }

        public IEnumerable<Comment> getAllbyPostID(int id)
        {
            return _db.Comments.Where(x => x.PostID == id).OrderByDescending(x=>x.CommentID);
        }

        public IEnumerable<SubComment> getAllSubCom(int id)
        {
            return _db.SubComments.Where(x=>x.CommentID==id).OrderByDescending(x=>x.SubCommentID);
        }

        public bool post_subComment(SubComment_View_Model sub)
        {
            try
            {
                var subcomment = new SubComment()
                {
                    Content = sub.Content,
                    CommentID = sub.CommentID,
                    DateSub = DateTime.Now
                };
                _db.SubComments.Add(subcomment);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
