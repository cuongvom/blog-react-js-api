﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.ModelViews;

namespace WebApplication1.Services
{
    public interface IAccountService
    {
        Task<bool> user_LoginAsync(LoginViewModel login);
        Task<bool> user_RegisterAsync(RegisterViewModel register);
        Task<bool> change_PasswordAsync(Account_View_Model acc);
    }
}
