﻿using System.Collections.Generic;
using WebApplication1.Models;
using WebApplication1.ModelViews;

namespace WebApplication1.Services
{
    public interface ICommentService
    {
        IEnumerable<Comment> getAllbyPostID(int id);
        bool create_Comm(Comment_View_Model comm);
        bool delete_Comm(int id);
        bool post_subComment(SubComment_View_Model sub);
        IEnumerable<SubComment> getAllSubCom(int id);
    }
}
