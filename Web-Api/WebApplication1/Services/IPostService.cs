﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Controllers;
using WebApplication1.Models;

namespace WebApplication1.Services
{
    public interface IPostService
    {
        IEnumerable<Post> getAllPostAsync();
        Post getPost_byID(int id);
        IEnumerable<Post> getPost_by_UserID(string id);
        bool create_NewPost(PostViewModel post);
        bool delete_Post(int id);
        bool update_Post(Post_Update_View_Model post);
    }
}
