﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using WebApplication1.Controllers;
using WebApplication1.Models;

namespace WebApplication1.Services
{
    public class PostService:IPostService
    {
        private readonly ApplicationContext _db;
        public PostService() { }
        public PostService(ApplicationContext db)
        {
            _db = db;
        }
        public IEnumerable<Post> getAllPostAsync()
        {
            return _db.Posts.OrderByDescending(x => x.PostID);
        }
        public IEnumerable<Post> getPost_by_UserID(string id)
        {
            return _db.Posts.Where(x=>x.UserID==id).OrderByDescending(x=>x.PostID);
        }
        public Post getPost_byID(int id)
        {
            return _db.Posts.Find(id);
        }
        public bool create_NewPost(PostViewModel post)
        {
            try
            {
                if (post == null)
                    return false;
                var file = post.Image;
                Post data = new Post();
                if (file != null)
                {
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string path = Path.Combine("Images", filename);
                    using (var fs = new FileStream(path, FileMode.Create))
                    {
                        file.CopyToAsync(fs);
                        data.Image = filename;
                    }
                }
                else
                {
                    data.Image = "";
                }
                data.Tittle = post.Tittle;
                data.Content = post.Content;
                data.SumComment = 0;
                data.DatePost = DateTime.Now;
                data.DateUpdate = DateTime.Now;
                data.UserID = post.UserID;
                data.CategoryID = post.CategoryID;
                data.UserName = post.UserName;
                _db.Posts.Add(data);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool delete_Post(int id)
        {
            try
            {
                if (id != 0)
                {
                    var post = _db.Posts.Find(id);
                    if (post != null)
                    {
                        _db.Posts.Remove(post);
                        _db.SaveChanges();
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool update_Post(Post_Update_View_Model post)
        {
            try
            {
                var file = post.Image;
                var data = _db.Posts.Find(post.PostID);
                if (file != null)
                {
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string path = Path.Combine("Images", filename);
                    using (var fs = new FileStream(path, FileMode.Create))
                    {
                        file.CopyToAsync(fs);
                        data.Image = filename;
                    }
                }
                data.Tittle = post.Tittle;
                data.Content = post.Content;
                data.DateUpdate = DateTime.Now;
                data.UserID = post.UserID;
                data.CategoryID = post.CategoryID;
                data.UserName = post.UserName;
                _db.Entry(data).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
