﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Services
{
    public class SearchService : ISearchService
    {
        private readonly ApplicationContext _db;
        public SearchService(ApplicationContext db)
        {
            _db = db;
        }
        public IEnumerable<Post> searchbyTitle(string search)
        {
            return _db.Posts.Where(x => x.Tittle.Contains(search))
                .OrderByDescending(x => x.PostID);
        }
    }
}
