﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication1;
using WebApplication1.Models;
using WebApplication1.ModelViews;
using WebApplication1.Services;

namespace XUnitTestProject3
{
    public class AccountServiceFake : IAccountService
    {
        private readonly UserManager<AppUser> _userM;
        public AccountServiceFake(UserManager<AppUser> userM)
        {
            _userM = userM;
        }

        public Task<bool> change_PasswordAsync(Account_View_Model acc)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> user_LoginAsync(LoginViewModel login)
        {
            try
            {
                var user = await _userM.FindByNameAsync(login.UserName);
                if (user != null)
                {
                    if (user.PasswordHash==login.Password)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            };
        }
        public async Task<bool> user_RegisterAsync(RegisterViewModel register)
        {
            try
            {
                var user = await _userM.FindByNameAsync(register.UserName);
                if (user != null)
                {
                    return false;
                }
                else
                {
                    _userM.CreateAsync(new AppUser { UserName = register.UserName }, register.Password).Wait();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
