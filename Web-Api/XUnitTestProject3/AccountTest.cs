﻿using Castle.Core.Configuration;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;
using System.Threading;
using WebApplication1;
using WebApplication1.Controllers;
using WebApplication1.Models;
using WebApplication1.Services;
using Xunit;

namespace XUnitTestProject3
{
    public class AccountTest
    {
        [Fact]
        public async System.Threading.Tasks.Task login_trueAsync()
        {
            var userstore = new Mock<IUserStore<AppUser>>();
            userstore.Setup(x => x.FindByNameAsync("user", CancellationToken.None)).ReturnsAsync(
                new AppUser()
                {
                    UserName = "user",
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    PasswordHash = "12345"
                }
                );
            var userManager = new UserManager<AppUser>(userstore.Object, null, null
                , null, null, null, null, null, null);
            var service = new AccountServiceFake(userManager);
            var login = new LoginViewModel()
            {
                UserName = "user",
                Password = "12345",
                Rememberme = false
            };
            var result = await service.user_LoginAsync(login);
            Assert.True(result);
        }
        [Theory]
        [InlineData("123456")]
        public async System.Threading.Tasks.Task login_falseAsync(string pass)
        {
            var userstore = new Mock<IUserStore<AppUser>>();
            userstore.Setup(x => x.FindByNameAsync("user", CancellationToken.None)).ReturnsAsync(
                new AppUser()
                {
                    UserName = "user",
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    PasswordHash = "12345"
                }
                );
            var userManager = new UserManager<AppUser>(userstore.Object, null, null
                , null, null, null, null, null, null);
            var service = new AccountService(userManager);
            var login = new LoginViewModel()
            {
                UserName = "user",
                Password = pass,
                Rememberme = false
            };
            var result = await service.user_LoginAsync(login);
            Assert.False(result);
        }
        [Fact]
        public async System.Threading.Tasks.Task register_trueAsync()
        {
            var userM = new FakeUserManager();
            var service = new AccountService(userM);
            var register = new RegisterViewModel()
            {
                UserName = "user1",
                Password = "12345"
            };
            var result = await service.user_RegisterAsync(register);
            Assert.True(result);
        }
        [Fact]
        public async System.Threading.Tasks.Task register_false_lenghtpasswordfaileAsync()
        {
            var userM = new FakeUserManager();
            var service = new AccountService(userM);
            var register = new RegisterViewModel()
            {
                UserName = "user1",
                Password = "123"
            };
            var result = await service.user_RegisterAsync(register);
            Assert.False(result);
        }
    }
}
