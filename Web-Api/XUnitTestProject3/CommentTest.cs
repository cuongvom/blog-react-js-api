﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication1;
using WebApplication1.Controllers;
using WebApplication1.Models;
using WebApplication1.Services;
using Xunit;

namespace XUnitTestProject3
{
    public class CommentTest
    {
        private  CommentController _controller;
        [Fact]
        public void createComment_true_postexits()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "createComment_true_postexits")
                .Options;
            using(var db=new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                });
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                var service = new CommentService(db);
                var comment = new Comment_View_Model()
                {
                    Content = "test",
                    PostID = 1,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                };
                var result = service.create_Comm(comment);
                Assert.True(result);
            }
        }
        [Fact]
        public void createComment_false_postnotexits()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "createComment_false_postnotexits")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                var service = new CommentService(db);
                var comment = new Comment_View_Model()
                {
                    Content = "test",
                    PostID = 1,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                };
                var result = service.create_Comm(comment);
                Assert.False(result);
            }
        }
        [Fact]
        public void getAll_comment_postid_1()
        {
            var options=new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "getAll_comment_postid_1")
                .Options;
            using (var db=new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                });
               
                db.Comments.Add(new Comment(){
                    Content="test",
                    DateCom=DateTime.Now,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user",
                    PostID=1
                });
                db.Comments.Add(new Comment()
                {
                    Content = "test1",
                    DateCom = DateTime.Now,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user",
                    PostID = 1
                });
                db.SaveChanges();
            }
            using (var db=new ApplicationContext(options))
            {
                var service = new CommentService(db);
                var result = service.getAllbyPostID(1).ToList().Count;
                Assert.Equal(2,result);
            }
        }
        [Fact]
        public void getAll_comment_postid_2()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "createComment_true_postnotexits")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                var service = new CommentService(db);
                var result = service.getAllbyPostID(2).ToList().Count;
                Assert.Equal(0, result);
            }
        }
        [Fact]
        public void controller_getAll_comment_postid_1()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "controller_getAll_comment_postid_1")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                });

                db.Comments.Add(new Comment()
                {
                    Content = "test",
                    DateCom = DateTime.Now,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user",
                    PostID = 1
                });
                db.Comments.Add(new Comment()
                {
                    Content = "test1",
                    DateCom = DateTime.Now,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user",
                    PostID = 1
                });
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                _controller = new CommentController(db);
                var result = _controller.Get(1).Result as OkObjectResult;
                var items = Assert.IsType<List<Comment>>(result.Value);
                Assert.Equal(2,items.Count);
            }
        }
        [Fact]
        public void controller_getAll_comment_postid_3()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "controller_getAll_comment_postid_3")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                _controller = new CommentController(db);
                var result = _controller.Get(3).Result as OkObjectResult;
                var items = Assert.IsType<List<Comment>>(result.Value);
                Assert.Empty(items);
            }
        }
        [Fact]
        public void controller_createComment_true_postexits()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "controller_createComment_true_postexits")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                });
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                _controller = new CommentController(db);
                var comment = new Comment_View_Model()
                {
                    Content = "test",
                    PostID = 1,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                };
                var result = _controller.Post(comment);
                Assert.IsType<OkObjectResult>(result);
            }
        }
        [Fact]
        public void controller_createComment_badrequest_postnotexits()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "controller_createComment_badrequest_postnotexits")
                .Options;
           
            using (var db = new ApplicationContext(options))
            {
                _controller = new CommentController(db);
                var comment = new Comment_View_Model()
                {
                    Content = "test",
                    PostID = 4,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user"
                };
                var result = _controller.Post(comment);
                Assert.IsType<BadRequestResult>(result);
            }
        }
        [Fact]
        public void controller_createComment_badrequest_modelnoinvalid()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "controller_createComment_badrequest_modelnoinvalid")
                .Options;

            using (var db = new ApplicationContext(options))
            {
                _controller = new CommentController(db);
                var comment = new Comment_View_Model()
                {
                    Content = null,
                    PostID = 100,
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = null
                };
                var result = _controller.Post(comment);
                Assert.IsType<BadRequestResult>(result);
            }
        }
    }
}
