﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication1;

namespace XUnitTestProject3
{
    public class FakeUserManager : UserManager<AppUser>
    {
        public FakeUserManager() : base(new Mock<IUserStore<AppUser>>().Object,
                  new Mock<IOptions<IdentityOptions>>().Object,
                  new Mock<IPasswordHasher<AppUser>>().Object,
                  new IUserValidator<AppUser>[0],
                  new IPasswordValidator<AppUser>[0],
                  new Mock<ILookupNormalizer>().Object,
                  new Mock<IdentityErrorDescriber>().Object,
                  new Mock<IServiceProvider>().Object,
                  new Mock<ILogger<UserManager<AppUser>>>().Object) { }
        public override Task<IdentityResult> CreateAsync(AppUser user, string password)
        {
            return Task.FromResult(IdentityResult.Success);
        }
        public override Task<AppUser> FindByNameAsync(string userName)
        {
            return Task.FromResult(It.IsAny<AppUser>());
        }
        public override Task<IList<string>> GetRolesAsync(AppUser user)
        {
            return Task.FromResult<IList<string>>(new List<string>() {"Admin","Member"});
        }
    }
}
