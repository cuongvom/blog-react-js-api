using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using WebApplication1;
using WebApplication1.Controllers;
using WebApplication1.Models;
using WebApplication1.Services;
using Xunit;

namespace XUnitTestProject3
{
    public class PostTest
    {
        [Fact]
        public void getAll_post()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "getAll_post")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test1"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test2",
                    Content = "test2",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test2"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test3",
                    Content = "test3",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cf3af25b-14a6-48bc-73df-08d7590d7d4e",
                    UserName = "test3"
                });
                db.SaveChanges();
            }
            using (var db=new ApplicationContext(options))
            {
                //Service
                var s = new PostService(db);
                var controller = new PostController(db);
                var r = s.getAllPostAsync().ToList().Count;
                //Controller
                var cr = controller.Get().Result as OkObjectResult;
                var item = Assert.IsType<List<Post>>(cr.Value);
                Assert.Equal(3,item.Count);
            }
        }
        [Fact]
        public void getPost_by_postID_notnull()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "getPost_by_postID_notnull")
                .Options;
            using (var db=new ApplicationContext(options))
            {
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test1"
                });
                db.SaveChanges();
                var s = new PostService(db);
                var r = s.getPost_byID(1);
                Assert.NotNull(r);
            }
        }
        [Fact]
        public void getPost_by_postID_null()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "getPost_by_postID_null")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                var s = new PostService(db);
                var r = s.getPost_byID(100);
                Assert.Null(r);
            }
        }
        [Fact]
        public void create_newpost_true()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "create_newpost_true")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName="user"
                });
                db.SaveChanges();
                var post = new PostViewModel
                {
                    Tittle = "test4",
                    Content = "test4",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user",
                    Image=null
                };
                var s = new PostService(db);
                var r = s.create_NewPost(post);
                Assert.True(r);
            }
        }
        [Fact]
        public void create_newpost_false_nullobj()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "create_newpost_false_nullobj")
                .Options;
            using (var db=new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.SaveChanges();
               PostViewModel post=null;
                var s = new PostService(db);
                var r = s.create_NewPost(post);
                Assert.False(r);
            }
        }
        [Fact]
        public void create_newpost_true_imagenotnull()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "create_newpost_true_imagenotnull")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.Categories.Add(new Category() { 
                CategoryName="test"
                });
                db.SaveChanges();
                var fileMock = new Mock<IFormFile>();
                //Setup mock file using a memory stream
                var s = "Hello World from a Fake File";
                var ms = new MemoryStream();
                var writer = new StreamWriter(ms);
                writer.Write(s);
                writer.Flush();
                ms.Position = 0;
                fileMock.Setup(m => m.OpenReadStream()).Returns(ms);
                fileMock.Setup(x => x.FileName).Returns("test.jpeg");
                fileMock.Setup(x => x.Name).Returns("image");
                fileMock.Setup(x => x.Length).Returns(10);
                fileMock.Setup(x => x.ContentType).Returns("image/png");
                var dis = "form-data;name='image';filename='test.jpeg'";
                fileMock.Setup(x => x.ContentDisposition).Returns(dis);

                var file = fileMock.Object;
                PostViewModel post = new PostViewModel() { 
                    Tittle="test",
                    Content="test",
                    UserID= "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName="user",
                    CategoryID=1,
                    Image=file
                };
                var service = new PostService(db);
                var r = service.create_NewPost(post);
                Assert.True(r);
            }
        }
        [Fact]
        public void delete_post_true()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "delete_post_true")
                .Options;
            using (var db=new ApplicationContext(options))
            {
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test1"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test2",
                    Content = "test2",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test2"
                });
               
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                var s = new PostService(db);
                var r = s.delete_Post(2);
                Assert.True(r);
            }
        }
        [Fact]
        public void delete_post_false_postid_0()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "delete_post_false_postid_0")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                var s = new PostService(db);
                var r = s.delete_Post(0);
                Assert.False(r);
            }
        }
        [Fact]
        public void delete_post_false_postid_no_invalid()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "delete_post_false_postid_no_invalid")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test1"
                });
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                var s = new PostService(db);
                var r = s.delete_Post(100);
                Assert.False(r);
            }
        }
        [Fact]
        public void update_post_true_file_null()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "update_post_true_null")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test1"
                });
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                var post = new Post_Update_View_Model
                {
                    Tittle = "update",
                    Content = "update",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user",
                    Image = null,
                    PostID=1
                };
                var s = new PostService(db);
                var r = s.update_Post(post);
                Assert.True(r);
            }
        }
        [Fact]
        public void update_post_true_file_notnull()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "update_post_true_file_notnull")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test1"
                });
                db.AppUsers.Add(new AppUser
                {
                    Id = new Guid("cb614d74-d3c1-4dd7-73e0-08d7590d7d4e"),
                    UserName = "user"
                });
                db.SaveChanges();
            }
            using (var db = new ApplicationContext(options))
            {
                var fileMock = new Mock<IFormFile>();
                //Setup mock file using a memory stream
                var s = "Hello World from a Fake File";
                var ms = new MemoryStream();
                var writer = new StreamWriter(ms);
                writer.Write(s);
                writer.Flush();
                ms.Position = 0;
                fileMock.Setup(m => m.OpenReadStream()).Returns(ms);
                fileMock.Setup(x=>x.FileName).Returns("test.jpeg");
                fileMock.Setup(x=>x.Name).Returns("image");
                fileMock.Setup(x => x.Length).Returns(10);
                fileMock.Setup(x=>x.ContentType).Returns("image/png");
                var dis = "form-data;name='image';filename='test.jpeg'";
                fileMock.Setup(x=>x.ContentDisposition).Returns(dis);

                var file = fileMock.Object;
                var post = new Post_Update_View_Model
                {
                    Tittle = "update",
                    Content = "update",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "user",
                    Image = file,
                    PostID = 1
                };
                var service = new PostService(db);
                var r = service.update_Post(post);
                Assert.True(r);
            }
        }
    }
}
