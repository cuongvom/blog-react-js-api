﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication1;
using WebApplication1.Controllers;
using WebApplication1.Services;
using Xunit;

namespace XUnitTestProject3
{
    public class SearchTest
    {
        [Fact]
        public void search_with_posttitle()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "search_post")
                .Options;
            using (var db = new ApplicationContext(options))
            {
                db.Posts.Add(new Post
                {
                    Tittle = "test1",
                    Content = "test1",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test1"
                });
                db.Posts.Add(new Post
                {
                    Tittle = "test2",
                    Content = "test2",
                    DatePost = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Image = "",
                    UserID = "cb614d74-d3c1-4dd7-73e0-08d7590d7d4e",
                    UserName = "test2"
                });
                db.SaveChanges();
            }
            using (var db=new ApplicationContext(options))
            {
                var controller = new SearchController(db);
                var service = new SearchService(db);
                var result = controller.Get("t").Result as OkObjectResult;
                var items = Assert.IsType<List<Post>>(result.Value);
                Assert.Equal(2,items.Count);
                Assert.Equal(2, service.searchbyTitle("t").ToList().Count);
            }
        }
    }
}
