import React from 'react';
import './App.css';
import Headerpage from './components/Headerpage';
import Footerpage from './components/Footerpage';
import Homebody from './components/Homebody';
import Login from './components/Login';
import NewPost from './components/NewPost';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Register from './components/Register';
import PostsManager from './components/PostsManager';
import Logout from './components/Logout';
import UpdatePost from './components/UpdatePost';
import PostDetail from './components/PostDetail';
import Profile from './components/Profile';
import ChangePassword from './components/ChangePassword';
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      token: localStorage.getItem("access_token"),
      id:'',
      url:'/'
    }
    this.setStateLogin = this.setStateLogin.bind(this);
    this.returnURL=this.returnURL.bind(this);
    this.handleLogout=this.handleLogout.bind(this);
  }
  setStateLogin(data) {
    if (data !== null || data !== "") {
      this.setState({...this.state,token:data}) 
    }
  }
  handleLogout(){
      localStorage.removeItem("access_token");
      this.setState({...this.state,token:null});
  }
  returnURL(url){
    this.setState({...this.state,url:url});
  }
  render() {
    return (
      <div className="App">
        <Router>
          <Headerpage token={this.state.token} handleLogout={this.handleLogout} returnURL={this.returnURL}>></Headerpage>
          <Switch>
            <Route exact path="/">
              <Homebody></Homebody>
            </Route>
            <Route path="/login">
              <Login setStateLogin={this.setStateLogin} returnURL={this.state.url}></Login>
            </Route>
            <Route path="/register">
              <Register></Register>
            </Route>
            <Route path="/posts">
              <PostsManager returnURL={this.returnURL}></PostsManager>
            </Route>
            <Route path="/post">
              <NewPost></NewPost>
            </Route>
            <Route path="/update_post/:id" component={UpdatePost}>
            </Route>
            <Route path="/detail/:id" component={props=><PostDetail returnURL={this.returnURL} {...props}/>}/>
            <Route path="/logout">
              <Logout handleLogout={this.handleLogout} returnURL={this.returnURL}/>
            </Route>
            <Route path="/profile" component={Profile}/>
            <Route path="/changepassword" component={ChangePassword}/>
          </Switch>
          <Footerpage></Footerpage>
        </Router>

      </div>
    );
  }
}

export default App;
