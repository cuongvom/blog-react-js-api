import axios from 'axios';
import * as Todo_actions from '../actions/Todo_actions';
import {GET_POST_BY_ID} from '../actions/Actions_type';
jest.mock('axios',()=>({
    get:jest.fn().mockResolvedValue({ data: { id:1,name:"test" },status:200})
}));
test('mock axios get',async ()=>{
    const response=await axios.get('');
    expect(response.status).toEqual(200);
});
jest.mock('axios',()=>({
    get:jest.fn().mockResolvedValue({data:{id:1,title:"test"},status:200})
}));
test('mock get post',async ()=>{
    const response=await axios.get('');
    expect(response.data).toEqual({id:1,title:"test"});
});
test('mock reducer',()=>{
    const action={
        type:GET_POST_BY_ID
    }
})