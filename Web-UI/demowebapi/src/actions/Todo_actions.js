import {
    DELETE_COMMENT, CHANGE_PASSWORD,
    GET_LIST_COMMENT, GET_POST_BY_ID,
    URL_RETURN,POST_SUBCOMMENT,
    GET_ALL_SUB_COMMENT
} from '../actions/Actions_type';
import { URL_SERVER, URL_FILE_SERVER } from '../actions/Initdata';
import axios from 'axios';
export const changePassword = (acc) => {
    return (dispatch) => {
        const token = 'Bearer '.concat(localStorage.getItem("access_token"));
        axios.defaults.headers.common['Authorization'] = token;
        axios.post(`${URL_SERVER}account/profile/changepassword`, acc)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    dispatch({
                        type: CHANGE_PASSWORD,
                    });
                    alert("Success.");
                }
            })
            .catch(err => {
                alert("Old password is wrong.");
                console.log(err);
            });
    }
}
export const delete_Comm = (id, index) => {
    return (dispatch) => {
        const token = 'Bearer '.concat(localStorage.getItem("access_token"));
        axios.defaults.headers.common['Authorization'] = token;
        axios.post(`${URL_SERVER}comment/delete/${id}`)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    dispatch({
                        type: DELETE_COMMENT,
                        index: index
                    });
                }
            })
            .catch(err => {
                console.log(err);
            })
    }
}
export const getComm = (id) => {
    return (dispatch) => {
        axios.get(`${URL_SERVER}comment/${id}`)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    //const data=JSON.parse(JSON.stringify(res.data));
                    dispatch({
                        type: GET_LIST_COMMENT,
                        listcomm: res.data
                    });
                }
            })
    }
}
export const getPostbyID = (id) => {
    return (dispatch) => {
        axios.get(`${URL_SERVER}post/${id}`)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    const data = res.data;
                    dispatch({
                        type: GET_POST_BY_ID,
                        post: data,
                        image_postdetail: URL_FILE_SERVER + data.image
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }
}
export const setURLReturn=(url)=>{
    return (dispatch)=>{
        dispatch({
            type:URL_RETURN,
            url_return:url
        });
    }
}
export const post_Subcomment=(subcomment)=>{
    return (dispatch)=>{
        const token = 'Bearer '.concat(localStorage.getItem("access_token"));
        axios.defaults.headers.common['Authorization'] = token;
        axios.post(`${URL_SERVER}comment/subcomment`,JSON.stringify(subcomment))
        .then(res=>{
            dispatch({
                type:POST_SUBCOMMENT
            });
        })
        .catch(err=>{
            console.log(err);
        })
    }
}
export const get_AllsubComm=(commentid)=>{
    return (dispatch)=>{
        axios.get(`${URL_SERVER}comment/subcomment/${commentid}`)
        .then(res=>{
            if(res.status>=200&&res.status<300){
                const data=res.data;
                dispatch({
                    type:GET_ALL_SUB_COMMENT,
                    listsubcomm:data.listcomm
                });
            }
        })
    }
}