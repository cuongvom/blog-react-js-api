import React, { useState, useEffect } from 'react';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';
import decode from 'jwt-decode';
import { Link, Redirect} from 'react-router-dom';
import { useDispatch} from 'react-redux';
import { changePassword } from '../actions/Todo_actions';
const ChangePassword = () => {
    const [password, setPassword] = useState("");
    const [newpassword, setNewPassword] = useState("");
    const token = localStorage.getItem("access_token");
    const [change_status,setChange]=useState(false);
    const dispatch = useDispatch();
    useEffect(() => {
        console.log(change_status);
    });
    const handleSubmit = () => {
        if (token != null) {
            const decoded = decode(token);
            const data = JSON.parse(JSON.stringify(decoded));
            const acc = {
                username: data.sub,
                password: password,
                newpassword: newpassword
            }
            dispatch(changePassword(acc));
            setChange(true);
        }
    }
    if(change_status)
    return(<Redirect to="/profile"/>);
    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Change password
      </Header>
                <Form size='large' onSubmit={handleSubmit}>
                    <Segment stacked>
                        <Form.Input fluid icon='lock' iconPosition='left' placeholder='Old password'
                            name='password' onChange={(e) => { setPassword(e.target.value) }}
                            required
                            value={password}
                            type="password" />
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='New password'
                            type='password'
                            name='newpassword'
                            required
                            onChange={(e) => { setNewPassword(e.target.value) }}
                            value={newpassword}
                        />
                        <Button color='teal' fluid size='large' type='submit'>
                            Submit
          </Button>
                    </Segment>
                </Form>
                <Message>
                    Back to login? <Link to='/login'>Login</Link>
                </Message>
            </Grid.Column>
        </Grid>
    );
}
export default ChangePassword;