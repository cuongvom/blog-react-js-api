import { URL_FRONT_END } from '../actions/Initdata';
import React from 'react';
import DeleteComment from './DeleteComment';
import Reply from '../components/Reply';
import ListSubComment from './ListSubComment';
//import {useSelector,useDispatch} from 'react-redux';
const Comment = ({ item, index }) => {
    
    return (
        <li className="media">
            <img src={URL_FRONT_END + "images/avatar.png"} alt="" className="img-circle pull-left" />
            <div className="media-body">
                <span className="text-muted pull-right">
                    <small className="text-muted">{item.dateCom}</small>
                </span><br />
                <strong className="text-success">{item.userName}</strong>
                <p className="offset-sm-1">
                    <span><small><DeleteComment item={item} index={index} /><Reply /></small></span><br />
                    {item.content}
                </p>
                <ListSubComment commentid={item.commentID}/>
            </div>
        </li>
    );
}
export default Comment;