import React from 'react';
import decode from 'jwt-decode';
import { useDispatch } from 'react-redux';
import { delete_Comm } from '../actions/Todo_actions';
import {URL_FRONT_END} from '../actions/Initdata';
const DeleteComment = ({ item, index }) => {
    const token = localStorage.getItem("access_token");
    const dispatch = useDispatch();
    const handDelete = () => {
        const confirm = window.confirm("Are you sure?");
        if (confirm) {
            dispatch(delete_Comm(item.commentID, index));
        }
    }
    if (token != null) {
        const data = JSON.parse(JSON.stringify(decode(token)));
        if (data.sub === item.userName)
            return (
                <img onClick={handDelete} className="icon-delete-comment"
                alt="delete comment"
                src={URL_FRONT_END+"/images/icondelete.png"}
                style={{height:20,width:20}}/>
            );
    }
    return(null);
}
export default DeleteComment;