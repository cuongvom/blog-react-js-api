import React from 'react';
import decode from 'jwt-decode';
import { Link } from 'react-router-dom';
class Headerpage extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnclick = this.handleOnclick.bind(this);
    this.handleLogin=this.handleLogin.bind(this);
  }
  handleOnclick() {
    this.props.handleLogout();
  }
  handleLogin(){
    this.props.returnURL("/");
  }
  render() {
    let token = this.props.token;
    if (token != null) {
      let decodded = decode(token);
      let user = JSON.parse(JSON.stringify(decodded));
      return (
        <header className="site-navbar" role="banner">
          <div className="site-wrap">
            <div className="container-fluid">
              <div className="row align-items-center">
                <div className="col-4 site-logo">
                  <Link to="/" className="text-black h2 mb-0">Blog app</Link>
                </div>
                <div className="col-8 text-right">
                  <nav className="site-navigation" role="navigation">
                    <ul className="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0">
                      <li><Link to="/" onClick={this.handleLogin}>Home</Link></li>
                      <li><Link to="/posts">My posts</Link></li>
                      <li><Link to="/profile">Hi {user.sub}!</Link></li>
                      <li><Link to="/" onClick={this.handleOnclick}>Logout</Link></li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>

        </header>
      );
    }
    return (
      <header className="site-navbar" role="banner">
        <div className="site-wrap">
          <div className="container-fluid">
            <div className="row align-items-center">
              <div className="col-4 site-logo">
                <Link to="/" className="text-black h2 mb-0">Blog app</Link>
              </div>
              <div className="col-8 text-right">
                <nav className="site-navigation" role="navigation">
                  <ul className="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/posts">My posts</Link></li>
                    <li><Link to="/register">Register</Link></li>
                    <li><Link to="/login" onClick={this.handleLogin}>Login</Link></li>

                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>

      </header>
    );
  }

}
export default Headerpage