import React from 'react';
import Post from './Post';
import axios from 'axios';
import {URL_SERVER} from '../actions/Initdata';
class Homebody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      currentPage: 1,
      total_data_fill: 3,
      search: ''
    }
    this.getAllpost = this.getAllpost.bind(this);
    this.handleOnchange = this.handleOnchange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }
  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }
  componentDidMount() {
    this.getAllpost();
  }
  async getAllpost() {
    await axios.get(`${URL_SERVER}post/get`)
      .then(res => {
        if (res.status >= 200 && res.status <= 300) {
          this.setState({ ...this.state, list: JSON.parse(JSON.stringify(res.data)) });
        }
      })
      .catch(err => {
          alert("Server disconnected.");
      });
  }
  handleOnchange(e) {
    let value = e.target.value;
    if(value==="")
      this.getAllpost();
    this.setState({ ...this.state, search: value });
  }
  handleSearch(e) {
    e.preventDefault();
    let value = this.state.search;
    if(value!==""){
      axios.get(`${URL_SERVER}search?search=${value}`)
      .then(res => {
        if (res.status >= 200 && res.status <= 300) {
          this.setState({ ...this.state, list: JSON.parse(JSON.stringify(res.data)) });
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
  }
  render() {
    const { list, currentPage, total_data_fill } = this.state;
    const indexOfLastTodo = currentPage * total_data_fill;
    const indexOfFirstTodo = indexOfLastTodo - total_data_fill;
    const currentTodos = list.slice(indexOfFirstTodo, indexOfLastTodo);
    const renderTodos = currentTodos.map((item) => {
      return <Post item={item} key={item.postID} />
    });
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(list.length / total_data_fill); i++) {
      pageNumbers.push(i);
    }
    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <button
          key={number}
          id={number}
          onClick={this.handleClick.bind(this)}
        >
          {number}
        </button>
      );
    });
    return (
      <div className="site-wrap">
        <div className="site-section">
          <div className="container">
            <div className="row mb-5">
              <form onSubmit={this.handleSearch}>
                <input type='text'
                  placeholder='Search...'
                  style={{ marginLeft: 70, width: 300, height: 30 }}
                  value={this.state.search}
                  onChange={this.handleOnchange}
                  name="search" />&nbsp;
                <input type='submit' value='Search'
                style={{height:30}}/>
              </form>
              <div className="col-12">
                <h2>Recent Posts</h2>
              </div>
            </div>
            <div className="row">
              {
                renderTodos
                // this.state.list.map(item => {
                //   return (
                //     <Post item={item} key={item.postID} />
                //   );
                // })
              }
            </div>
            <div className="row text-center pt-5 border-top">
              <div className="col-md-12">
                <div className="custom-pagination">
                  {renderPageNumbers}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

  }
}
export default Homebody;