import React from 'react';
import Comment from '../components/Comment';
const ListComment = ({list}) => {
    return (
        <div className="list-comment">
            <div className="comment-wrapper">
                <ul className="media-list">
                    {
                        list.map((item, index) => {
                            return (
                                <Comment key={item.commentID} item={item} index={index} />
                            );
                        })
                    }
                </ul>
            </div>
        </div>
    );
}
export default ListComment;