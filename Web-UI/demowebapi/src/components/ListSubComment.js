import React,{useEffect,useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { get_AllsubComm } from '../actions/Todo_actions';
const ListSubComment = ({ commentid }) => {
    const { listsubcomm } = useSelector(state => state.Todo_reducer);
    const dispatch=useDispatch();
    useEffect(()=>{
        dispatch(get_AllsubComm(commentid));
    },[commentid,dispatch]);
    console.log(listsubcomm);
    return (
        <div className="offset-sm-1">
            {
                listsubcomm.map(item => {
                    return (
                        <p key={item.subCommentID}>{item.content}</p>
                    )
                })
            }
        </div>
    );
}
export default ListSubComment;