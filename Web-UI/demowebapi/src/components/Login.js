import React from 'react';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';
import axios from 'axios';
import { Redirect,Link } from 'react-router-dom';
import { URL_SERVER } from '../actions/Initdata';
class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      rememberMe: false,
      loggedIn: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = event => {
    const value = event.target.value;
    this.setState({ [event.target.name]: value })
  }
  handleSubmit = event => {
    event.preventDefault();
    let account = {
      userName: this.state.userName,
      password: this.state.password,
      rememberMe: this.state.rememberMe
    };
    const LOGIN_ENDPOINT = `${URL_SERVER}account/login`;
    axios.post(LOGIN_ENDPOINT, JSON.stringify(account), { headers: { 'Content-Type': 'application/json' } })
      .then(res => {
        if (res.status >= 200 && res.status < 300) {
          let jwt = res.data.token;
          localStorage.setItem("access_token", jwt);
          this.setState({ ...this.state, loggedIn: true });
          this.props.setStateLogin(jwt);
        }
      })
      .catch(err => {
        alert("User name or password not exits.");
        console.log(err);
      });
  }
  render() {
    if (this.state.loggedIn) {
      return (<Redirect to={this.props.returnURL} />);
    }
    return (
      <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' color='teal' textAlign='center'>
            Log-in to your account
      </Header>
          <Form size='large' onSubmit={this.handleSubmit}>
            <Segment stacked>
              <Form.Input fluid icon='user' iconPosition='left' placeholder='User name'
                name='userName' onChange={this.handleChange}
                required />
              <Form.Input
                fluid
                icon='lock'
                iconPosition='left'
                placeholder='Password'
                type='password'
                name='password'
                required
                onChange={this.handleChange}
              />
              <Form.Checkbox label='Remember me.' name='rememberMe' onChange={this.handleChange}></Form.Checkbox>

              <Button color='teal' fluid size='large' type='submit'>
                Login
          </Button>
            </Segment>
          </Form>
          <Message>
            New to us? <Link to='/register'>Register</Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}
export default Login;    