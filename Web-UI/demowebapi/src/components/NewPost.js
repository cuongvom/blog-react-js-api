import React from 'react';
import { Button, Form, Segment, Header, Grid } from 'semantic-ui-react';
import axios from 'axios';
import decode from 'jwt-decode';
import {Redirect} from 'react-router-dom';
import {URL_SERVER} from '../actions/Initdata';
const token='Bearer '.concat(localStorage.getItem("access_token"));
axios.defaults.headers.common['Authorization']=token;
export default class NewPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            tittle: '',
            content: '',
            image: null,
            userid: '',
            username: '',
            categoryid: 1,
            status:false
        }
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setStateforUserID = this.setStateforUserID.bind(this);
    }
    componentDidMount() {
        axios.get(`${URL_SERVER}category/get`)
            .then(res => {
                let op = JSON.parse(JSON.stringify(res.data));
                this.setState({ ...this.state, options: op });
            });
        this.setStateforUserID();
    }
    setStateforUserID() {
        let token = localStorage.getItem("access_token");
        if (token != null) {
            let decoded = decode(token);
            let user = JSON.parse(JSON.stringify(decoded));
            this.setState({ ...this.state, userid: user.id, username: user.sub });
        }
    }
    handleChangeImage = (event) => {
        let file = event.target.files[0];
        this.setState({ ...this.state, image: file })
    }
    handleChange = (event) => {
        let value = event.target.value;
        this.setState({ ...this.state, [event.target.name]: value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const formdata = new FormData();
        formdata.append("image", this.state.image);
        formdata.append("userid", this.state.userid);
        formdata.append("tittle", this.state.tittle);
        formdata.append("content", this.state.content);
        formdata.append("categoryid", this.state.categoryid);
        formdata.append("username", this.state.username);
        axios.post(`${URL_SERVER}post/create`, formdata)
            .then(res => {
                if (res.status >= 200 && res.status <= 300) {
                    this.setState({...this.state,status:true});
                    alert("Successfully.");
                }
            });
    }
    render() {
        let status=this.state.status;
        if(status){
            return(<Redirect to="/posts"/>);
        }
        return (
            <Grid textAlign='center' style={{ height: '100vh' }}>
                <Grid.Column style={{ maxWidth: 700 }}>
                    <Header as='h2' color='teal' textAlign='center'>New post</Header>
                    <Form size='large' onSubmit={this.handleSubmit}>
                        <Segment stacked>
                            <Form.Input
                                placeholder='Tittle'
                                name='tittle'
                                onChange={this.handleChange}
                                required />
                            <Form.TextArea
                                placeholder='Content'
                                type='TextArea'
                                name='content'
                                required
                                style={{ height: '20vh' }}
                                onChange={this.handleChange}
                            />
                            <Form.Input
                                type='file'
                                name='image'
                                onChange={this.handleChangeImage}
                            />
                            <select value={this.state.categoryid} name='categoryid' onChange={this.handleChange} className='form-group'>
                                {this.state.options.map(op => {
                                    return (
                                        <option key={op.categoryID} value={op.categoryID}>{op.categoryName}</option>
                                    )
                                })}
                            </select>
                            <Button color='teal' fluid size='large' type='submit' style={{ maxWidth: '100px' }}>Post</Button>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>

        );
    }
}