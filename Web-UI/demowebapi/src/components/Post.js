import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {URL_SERVER,URL_FILE_SERVER, URL_FRONT_END} from '../actions/Initdata';
class Post extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: {},
            category: {}
        }
        this.getCategory = this.getCategory.bind(this);
    }
    componentDidMount() {
        this.setState({ ...this.state, item: this.props.item });
        this.getCategory();
    }
    getCategory() {
        let id = this.props.item;
        axios.get(`${URL_SERVER}category/${id.categoryID}`)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    let data = JSON.parse(JSON.stringify(res.data));
                    this.setState({ ...this.state, category: data });
                }
            });
    }
    render() {
        let image = this.state.item.image;
        if (image !=="")
            image = URL_FILE_SERVER+ image;
        return (
            <div className="col-lg-4 mb-4">
                <div className="entry2">
                    <div style={{ height: 250, width: 300}}>
                        <Link to={'/detail/' + this.state.item.postID}>
                            <img src={image} alt=""
                            className="img-fluid rounded"
                            style={{ height: 250, width: 300}}/>
                        </Link>
                    </div>
                    <div className="excerpt">
                        <span className="post-category text-white bg-secondary mb-3">{this.state.category.categoryName}</span>
                        <h2><Link to={'/detail/' + this.state.item.postID}>{this.state.item.tittle}</Link></h2>
                        <div className="post-meta align-items-center text-left clearfix">
                            <figure className="author-figure mb-0 mr-3 float-left"><img src={URL_FRONT_END+"images/avatar.png"} alt="" className="img-fluid" /></figure>
                            <span className="d-inline-block mt-1">By <Link to={'/detail/' + this.state.item.postID}>{this.state.item.userName}</Link></span>
                            <span>&nbsp;&nbsp;&nbsp;{this.state.item.datePost}</span>
                        </div>
                        <p>{this.state.item.content}</p>
                        <p><Link to={'/detail/' + this.state.item.postID}>Read More</Link></p>
                    </div>
                </div>
            </div>
        );
    }
}
export default Post;