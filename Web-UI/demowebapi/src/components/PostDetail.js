import React from 'react';
import axios from 'axios';
import decode from 'jwt-decode';
import { Redirect } from 'react-router-dom';
import { URL_SERVER} from '../actions/Initdata';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Todo_actions from '../actions/Todo_actions';
import ListComment from './ListComment';
class PostDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id:props.match.params.id,
            data: {},
            src: '',
            comment: '',
            logged: true,
        }
        this.getPost = this.getPost.bind(this);
        this.getAllComment = this.getAllComment.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleOnsubmit = this.handleOnsubmit.bind(this);
    }
    componentDidMount() {
        this.getPost();
        this.getAllComment();
    }
    getPost() {
        const { actions } = this.props;
        actions.getPostbyID(this.state.id);
    }
    getAllComment() {
        const { actions } = this.props;
        actions.getComm(this.state.id);
    }
    handleOnChange(e) {
        let value = e.target.value;
        this.setState({ comment: value });
    }
    handleOnsubmit(e) {
        e.preventDefault();
        let _token = localStorage.getItem("access_token");
        if (_token != null) {
            let token = 'Bearer '.concat(_token);
            axios.defaults.headers.common['Authorization'] = token;
            let user = JSON.parse(JSON.stringify(decode(_token)));
            let data = {
                postID: Number(this.state.id),
                userid: user.id,
                userName: user.sub,
                content: this.state.comment
            }
            axios.post(`${URL_SERVER}comment`,
                JSON.stringify(data),
                {
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
                .then(res => {
                    if (res.status >= 200 && res.status < 300) {
                        this.setState({ ...this.state, comment: "" });
                        this.getAllComment();
                        this.getPost();
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }
        else {
            alert("You need login");
            this.setState({ ...this.state, logged: false });
        }
    }
    render() {
        const { todos } = this.props;
        const{post,image_postdetail,listcomm}=todos;
        const logged = this.state.logged;
        if (!logged) {
            const id = this.state.id;
            this.props.returnURL(`/detail/${id}`);
            return (<Redirect to="/login" />);
        }
        return (
            <div className="postdetail">
                <sub>{post.dateUpdate} by {post.userName}</sub>
                <h3>{post.tittle}</h3>
                <p>
                    <img src={image_postdetail} alt={post.image} className="img-fluid rounded" height="300" width="250" />
                </p>
                <p style={{ marginLeft: '20px' }}>{post.content}</p>
                <p><sub>Comment: {post.sumComment}</sub></p>
                <div className="comment-box">
                    <form onSubmit={this.handleOnsubmit}>
                        <p>
                            <textarea
                                placeholder="Write comment...."
                                style={{ height: '10vh', width: '80vh' }}
                                className="comment-input"
                                value={this.state.comment}
                                onChange={this.handleOnChange}
                                required />
                        </p>
                        <button type="submit" className="form-group">Submit</button>
                    </form>
                    <hr style={{ width: '900px', float: 'left' }} /><br /><br />
                    <ListComment list={listcomm}/>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        todos: state.Todo_reducer
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Todo_actions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PostDetail);