import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import decode from 'jwt-decode';
import { Redirect } from 'react-router-dom';
import {URL_SERVER} from '../actions/Initdata';
export default class PostsManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: 0,
        }
        this.getAllpost = this.getAllpost.bind(this);
    }
    componentDidMount() {
        let token = localStorage.getItem("access_token");
        if (token != null) {
            this.getAllpost(token);
        }
        else{
            this.props.returnURL("/posts");
        }
    }
    getAllpost(_token) {
        let data = JSON.parse(JSON.stringify(decode(_token)));
        let user_id = data.id;
        let token = 'Bearer '.concat(localStorage.getItem("access_token"));
        axios.defaults.headers.common['Authorization'] = token;
        axios.get(`${URL_SERVER}post/GetPostbyUserId?id=${user_id}`)
            .then(res => {
                if (res.status >= 200 && res.status <= 300) {
                    this.setState({ ...this.state, list: JSON.parse(JSON.stringify(res.data)) });
                }
            })
            .catch(err => {
                alert(err);
            });
    }
    handleDelete(id) {
        let _token = localStorage.getItem("access_token");
        let token = 'Bearer '.concat(localStorage.getItem("access_token"));
        axios.defaults.headers.common['Authorization'] = token;
        if (id !== 0) {
            axios.delete(`${URL_SERVER}post/${id}`)
                .then(re => {
                    if (re.status >= 200 && re.status < 300) {
                        alert("Delete successfully");
                        this.getAllpost(_token);
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }

    render() {
        let token = localStorage.getItem("access_token");
        if (token == null) {
            return (<Redirect to="/login" />);
        }
        return (
            <div>
                <Link to='/post'>New post</Link>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Tittle</th>
                            <th>Content</th>
                            <th>Image</th>
                            <th>Date post</th>
                            <th>Date update</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.list.map(item => {
                            return (
                                <tr key={item.postID}>
                                    <td>{item.tittle}</td>
                                    <td>{item.content}</td>
                                    <td>{item.image}</td>
                                    <td>{item.datePost}</td>
                                    <td>{item.dateUpdate}</td>
                                    <td><Link className='btn btn-primary' to={"/update_post/" + item.postID}>Edit</Link></td>
                                    <td>
                                        <button className='btn btn-danger' onClick={
                                            () => {
                                                let r = window.confirm('Are you sure ?');
                                                if (r === true) {
                                                    this.handleDelete(item.postID);
                                                }
                                            }
                                        }>Delete</button>
                                    </td>

                                </tr>
                            )
                        }
                        )}

                    </tbody>

                </table>

            </div >
        );
    }
}