import React from 'react';
import { Redirect } from "react-router-dom";
import { Button, Form, Grid, Header, Segment} from 'semantic-ui-react';
import axios from 'axios';
import {URL_SERVER} from '../actions/Initdata';
class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      redirect: false,
      err: {
        password: ''
      }
    }
  }
  handleChange = event => {
    const { name, value } = event.target;
    let err = this.state.err;
    switch (name) {
      case 'password':
        err.password =
          value.lenght < 5
            ? 'Password must be 5 characters long!' :
            '';
        break;
      default: break;
    }
    this.setState({ err, [name]: value });
  }
  handleSubmit = event => {
    event.preventDefault();
    var pass=this.state.password;
    if(pass.length<5){
      alert("Password must >=5 characters.");
    }
    else
    {
      const account = {
        userName: this.state.userName,
        password: this.state.password
      }
  
      axios.post(`${URL_SERVER}account/register`,
        JSON.stringify(account),
        { headers: { 'Content-Type': 'application/json', } })
        .then(res => {
          if (res.status >= 200 && res.status < 300) {
            alert("Successfully");
            this.setState({ redirect: true });
          }
        }).catch(err => {
          alert("User name already.");
        });
    }
    
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to='/login'></Redirect>
    }
    return (

      <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' color='teal' textAlign='center'>
            Register
          </Header>
          <Form size='large' onSubmit={this.handleSubmit}>

            <Segment stacked>
              <Form.Input fluid icon='user'
                iconPosition='left'
                placeholder='User name'
                name="userName"
                value={this.state.userName}
                onChange={this.handleChange}
                required />
              <Form.Input
                fluid
                icon='lock'
                iconPosition='left'
                placeholder='Password'
                type='password'
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
                required
              />
              <Button color='teal' fluid size='large' type='submit'>
                Register
              </Button>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

export default Register;   