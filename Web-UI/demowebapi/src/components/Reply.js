import React from 'react';
import {URL_FRONT_END} from '../actions/Initdata';
const Reply = () => {
    const handleReply = () => {
        const token=localStorage.getItem("access_token");
        if(token!=null){
            const reply=window.prompt("Please enter your reply.","Reply...");
            console.log(reply);
        }
    }
    return (
        <img onClick={handleReply} className="icon-reply-comment" alt="reply" src={URL_FRONT_END+"/images/iconreply.png"}
        style={{height:20,width:20}}></img>
    );
}
export default Reply;