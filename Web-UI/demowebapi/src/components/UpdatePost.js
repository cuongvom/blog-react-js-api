import React from 'react';
import { Button, Form, Segment, Header, Grid } from 'semantic-ui-react';
import axios from 'axios';
import {Redirect} from 'react-router-dom';
import {URL_SERVER} from '../actions/Initdata';
export default class UpdatePost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            tittle: '',
            content: '',
            image: null,
            userid: '',
            username: '',
            categoryid: 1,
            postid:0,
            status:false
        }
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        //Get list category
        axios.get(`${URL_SERVER}category/get`)
            .then(res => {
                let op = JSON.parse(JSON.stringify(res.data));
                this.setState({ ...this.state, options: op });
            });
        //Get post by id
        const { id } = this.props.match.params;
        axios.get(`${URL_SERVER}post/${id}`)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    let data = JSON.parse(JSON.stringify(res.data));
                    this.setState({
                        tittle:data.tittle,
                        content:data.content,
                        categoryid:data.categoryID,
                        username:data.userName,
                        userid:data.userID,
                        postid:id
                    });
                }
            })

    }
    handleChangeImage = (event) => {
        this.setState({ ...this.state, image: event.target.files[0] })
    }
    handleChange = (event) => {
        let value = event.target.value;
        this.setState({ ...this.state, [event.target.name]: value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const formdata = new FormData();
        formdata.append("userid", this.state.userid);
        formdata.append("image", this.state.image);
        formdata.append("tittle", this.state.tittle);
        formdata.append("content", this.state.content);
        formdata.append("categoryid", this.state.categoryid);
        formdata.append("username", this.state.username);
        formdata.append("postid",this.state.postid);
        axios.put(`${URL_SERVER}post`, formdata)
            .then(res => {
                if (res.status >= 200 && res.status <= 300) {
                    alert("Successfully.");
                    this.setState({...this.state,status:true});
                }
            })
            .catch(err=>{
                console.log(err);
            });
    }
    render() {
        if(this.state.status){
            return(
                <Redirect to='/posts'/>
            );
        }
        return (
            <Grid textAlign='center' style={{ height: '100vh' }}>
                <Grid.Column style={{ maxWidth: 700 }}>
                    <Header as='h2' color='teal' textAlign='center'>Update post</Header>
                    <Form size='large' onSubmit={this.handleSubmit}>
                        <Segment stacked>
                            <Form.Input
                                placeholder='Tittle'
                                name='tittle'
                                onChange={this.handleChange}
                                required
                                value={this.state.tittle} />
                            <Form.TextArea
                                placeholder='Content'
                                type='TextArea'
                                name='content'
                                required
                                style={{ height: '20vh' }}
                                onChange={this.handleChange}
                                value={this.state.content}
                            />
                            <Form.Input
                                type='file'
                                name='image'
                                filename={this.state.image}
                                onChange={this.handleChangeImage}
                                
                            />
                            <select value={this.state.categoryid} name='categoryid' onChange={this.handleChange} className='form-group'>
                                {this.state.options.map(op => {
                                    return (
                                        <option key={op.categoryID} value={op.categoryID}>{op.categoryName}</option>
                                    )
                                })}
                            </select>
                            <Button color='teal' fluid size='large' type='submit' style={{ maxWidth: '100px' }}>Update</Button>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>

        );
    }
}