import {
    CHANGE_PASSWORD, DELETE_COMMENT
    , GET_LIST_COMMENT, GET_POST_BY_ID,
    URL_RETURN, POST_SUBCOMMENT,
    GET_ALL_SUB_COMMENT
} from '../actions/Actions_type';
const initstate = {
    listcomm: [],
    post: {},
    image_postdetail: '',
    url_return: '/',
    listsubcomm: []
}
const todos = (state = initstate, action) => {
    switch (action.type) {
        case CHANGE_PASSWORD:
            return {
                ...state
            };
        case DELETE_COMMENT:
            let post = state.post;
            post.sumComment--;
            return {
                ...state,
                listcomm: state.listcomm.filter((data, index) => index !== action.index),
                post: post
            }
        case GET_LIST_COMMENT:
            return {
                ...state,
                listcomm: action.listcomm
            }
        case GET_POST_BY_ID:
            return {
                ...state,
                post: action.post,
                image_postdetail: action.image_postdetail
            }
        case URL_RETURN:
            return {
                ...state,
                url_return: action.url_return
            }
        case POST_SUBCOMMENT:
            return {
                ...state
            }
        case GET_ALL_SUB_COMMENT:
            return {
                ...state,
                listsubcomm: action.listsubcomm
            }
        default: return state;
    }
}
export default todos;